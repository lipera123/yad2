from yad2_api import get_yad2_update, get_total_tlv_pages, get_yad2_page, filter_irrelevant
import json
from datetime import datetime
import os
from token_key import TOKEN

import logging
from telegram import Update
from telegram.ext import ApplicationBuilder, ContextTypes, CommandHandler
import time
import os 

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

LAST_FILE_PATH = "last_file.txt"

last_file = ""

async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    print("start called")
    await context.bot.send_message(chat_id=update.effective_chat.id, text="I'm a bot, please talk to me!")

def print_house_data(h):
    out = ""
    try:
        out += f"{h['neighborhood']}\n"
    except KeyError:
        pass
    out += f"street: {h['street']}\n"
    out += f"price: {h['price']}\n"
    out += f"rooms: {h['rooms']}\n"
    out += f"{h['updatedAt']}\n"
    out += f"{h['linkToken']}\n"
    return out

def combine_json_files(folder_name):
    # Get a list of all JSON files in the specified folder
    json_files = [f for f in os.listdir(folder_name) if f.endswith('.json')]

    # Initialize an empty list to store combined data
    combined_data = []

    # Read data from each JSON file and append it to the combined_data list
    for file_name in json_files:
        with open(os.path.join(folder_name, file_name), 'r') as infile:
            data = json.load(infile)
            combined_data.extend(data)

    # Create a new JSON file with the combined data

    time_now = datetime.now().strftime('%Y-%m-%d_%H-%M')

    # Create the file name
    output_file_name = f"final_output/{time_now}.json"

    # output_file_name = f"{datetime.now().strftime('%Y-%m-%d')}.json"
    with open(output_file_name, 'w') as outfile:
        json.dump(combined_data, outfile, ensure_ascii=False)

    # # Delete the original JSON files (except for the output file)
    # for file_name in json_files:
    #     if file_name != output_file_name:
    #         os.remove(os.path.join(folder_name, file_name))

    return output_file_name

async def update_data(update: Update, context: ContextTypes.DEFAULT_TYPE):

    while(True): 
        print("update_data called")
        await context.bot.send_message(chat_id=update.effective_chat.id, text=f"Updating time=[{datetime.now().strftime('%Y-%m-%d_%H-%M')}]")
        os.system("./curl_url.sh")
        page_num = get_total_tlv_pages()
        print(f"{page_num=}")

        for i in range(page_num+1):
            data, requst_hash = get_yad2_page(i)
            data = filter_irrelevant(data)
            with open(f"cached_pages/page_{i}.json", "w") as f:
                json.dump(data,f, ensure_ascii=False)
            # print_house_data(data)
            print(f"dumped to cached_pages/page_{i}")
            time.sleep(2)

        output_file = combine_json_files("cached_pages")

        last_file = load_last_file()

        if last_file != "":
            new_tokens = diff_files(output_file,last_file)

        save_last_file(output_file)
        print(f"Merged data written to '{output_file}'")
        await context.bot.send_message(chat_id=update.effective_chat.id, text="Update done!")

        if new_tokens:
            with open(output_file) as f:
                output_data = json.load(f)
                for idx,_ in new_tokens:
                    print(f"{idx=}")
                    print(f"{update.effective_chat.id=}")
                    await context.bot.send_message(chat_id=update.effective_chat.id, text=print_house_data(output_data[idx]))
                    time.sleep(5)
        time.sleep(3600)


def diff_files(cur,prev):
    new_tokens = []

    with open(cur) as new_fp:
        with open(prev) as old_fp:
    
            cur_data = json.load(new_fp)
            cur_tokens = [ token["linkToken"] for token in cur_data]

            prev_data = json.load(old_fp)
            prev_tokens = [ token["linkToken"] for token in prev_data]

            for idx,token in enumerate(cur_tokens):
                if token not in prev_tokens:
                    new_tokens.append((idx,token))

    return new_tokens


def save_last_file(path):
    with open(LAST_FILE_PATH,"w") as f:
        f.write(path)

def load_last_file():
    try:
        with open(LAST_FILE_PATH,"r") as f:
            return f.read().strip()
    except Exception as e:
        print(f"load_last_file(): {e}")
        return ""
        
def main():

    application = ApplicationBuilder().token(TOKEN).build()
    
    start_handler = CommandHandler('start',start)
    update_handler = CommandHandler('update',update_data)
    application.add_handler(start_handler)
    application.add_handler(update_handler)
    
    application.run_polling()
    # with open("example.json","r") as f:
    #     data = json.load(f)

    # data, requst_hash = get_yad2_update()
    # data = filter_irrelevant(data)
    # print_house_data(data)
    # print(f"{requst_hash=}")


if __name__ == "__main__":
    main()